import React from 'react'
import { Grid } from './components/common'
import { Register } from './components/main'
import { createMuiTheme, ThemeProvider } from '@material-ui/core'

import arialmt from './assets/fonts/arialmt.woff'
import { Provider } from 'react-redux'
import { configureStore } from './redux/store/store'

const ArialMT = {
  fontFamily: 'ArialMT',
  fontStyle: 'normal',
  fontDisplay: 'swap',
  src: `
    local('ArialMT'),
    local('ArialMT-Regular'),
    url(${arialmt}) format('woff2')
  `,
  unicodeRange:
    'U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF',
}

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#565656',
      light: '#757575',
      dark: '#206FCC',
    },
    secondary: {
      main: '#CE1126',
    },
    error: {
      main: '#CE1126',
    },
  },
  breakpoints: {
    values: {
      mobile: 1,
      desktop: 768,
    },
  },
  typography: {
    h2: {
      fontSize: '1.875rem',
      '@media (min-width:768px)': {
        fontSize: '3.75rem',
      },
    },
    subtitle1: {
      fontSize: '0.875rem',
      '@media (min-width:768px)': {
        fontSize: '1.125rem',
      },
    },
    subtitle2: {
      fontSize: '0.875rem',
    },
    fontFamily: 'ArialMT, Arial',
  },
  shape: {
    borderRadius: 0,
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        '@font-face': [ArialMT],
      },
    },
    MuiOutlinedInput: {
      input: { padding: 0, height: '2.1875rem' },
    },
  },
})

const store = configureStore()

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Grid container justify="center" alignItems="center">
          <Register />
        </Grid>
      </ThemeProvider>
    </Provider>
  )
}

export default App
