import React, { useState } from 'react'
import { Typography, TextField, Box } from '../common'
import { makeStyles } from '@material-ui/core'
import { useTheme } from '@material-ui/core/styles'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  fieldDesktop: {
    margin: theme.spacing(2),
    maxWidth: '32.25rem',
  },
  fieldMobile: {
    margin: `24px ${theme.spacing(1.875)}px`,
    maxWidth: '16.25rem',
    height: '2.5rem',
  },
}))

export const FormField = ({
  keyId,
  content,
  placeHolder,
  variant,
  typographyVariant,
  value = '',
  required,
  withInputField = true,
}) => {
  const classes = useStyles()
  const [input, setInput] = useState(value)

  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  const handleInputChange = (e) => {
    setInput(e.target.value)
  }
  console.log(input)

  const correct = true

  const textField = withInputField && (
    <TextField
      color={correct ? 'primary' : 'secondary'}
      style={{ background: 'white' }}
      required={required}
      fullWidth
      placeholder={placeHolder}
      value={input}
      variant={variant}
      handleChange={handleInputChange}
    />
  )

  const field = (
    <Box
      className={isDesktop ? classes.fieldDesktop : classes.fieldMobile}
      key={keyId}
    >
      <Typography
        color={correct ? 'primary' : 'error'}
        content={content}
        variant={typographyVariant}
      />
      {textField}
    </Box>
  )

  return field
}
