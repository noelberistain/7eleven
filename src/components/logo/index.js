import React from 'react'
import { CardMedia } from '../common'
import { makeStyles } from '@material-ui/core'
import logo from '../../assets/logo-desktop.svg'
import { useTheme } from '@material-ui/core/styles'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  mediaDesktop: {
    margin: theme.spacing(1),
    width: 182,
    height: 36,
  },
  mediaMobile: {
    margin: theme.spacing(1),
    width: 157,
    height: 28,
  },
}))

export const Logo = () => {
  const classes = useStyles()
  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  const Logo = (
    <CardMedia
      className={isDesktop ? classes.mediaDesktop : classes.mediaMobile}
      image={logo}
      title="Company Logo"
    />
  )

  return Logo
}
