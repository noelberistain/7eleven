import React, { useState } from 'react'
import { makeStyles } from '@material-ui/core'
import { DatePickers } from '../dateFormField'
import { useTheme } from '@material-ui/core/styles'
import { useMediaQuery } from '@material-ui/core'
import { CountryField } from '../countryField'
import { TermsAndNews } from '../termsField'
import { buildFields } from './buildFields'
import { Button } from '../common'
import { useDispatch } from 'react-redux'
import { registerNewUser } from '../../redux/actions/'

const useStyles = makeStyles((theme) => ({
  rootMobile: {
    backgroundColor: '#F7F7F7',
    maxWidth: 320,
    height: 735,
    paddingTop: 1,
  },
  rootDesktop: {
    backgroundColor: '#F7F7F7',
    maxWidth: 556,
    height: 735,
    paddingTop: 1,
  },
}))

export const RegisterForm = () => {
  const classes = useStyles()
  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  const dispatch = useDispatch()

  // local state for validation before saving it
  const [user, setUser] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: '',
    birthDate: '',
    phone: '',
    country: '',
    zipCode: '',
  })

  const headerField = [
    {
      content:
        'Enter your information below for exclusive offers, promotions and savings',
      typographyVariant: 'subtitle1',
      withInputField: false,
    },
  ]
  const fieldsData = [
    {
      content: '* Fields Required',
      typographyVariant: 'subtitle2',
      withInputField: false,
    },
    {
      content: 'First Name *',
      variant: 'outlined',
      typographyVariant: 'subtitle2',
      value: user.firstName,
    },
    {
      content: 'Last Name',
      variant: 'outlined',
      typographyVariant: 'subtitle2',
      value: user.lastName,
    },
    {
      content: 'Email Address',
      required: true,
      variant: 'outlined',
      typographyVariant: 'subtitle2',
      value: user.email,
    },
    {
      content: 'Choose Password *',
      required: true,
      variant: 'outlined',
      typographyVariant: 'subtitle2',
      value: user.password,
    },
    {
      content: 'Confirm Password *',
      required: true,
      variant: 'outlined',
      typographyVariant: 'subtitle2',
      value: user.confirmPassword,
    },
  ]
  const phoneField = [
    {
      content: 'Phone Number',
      placeHolder: '(XXX) XXX-XXXX',
      typographyVariant: 'subtitle2',
      variant: 'outlined',
      value: user.phone,
    },
  ]
  const zipCodeField = [
    {
      content: 'Zip/Postal Code *',
      required: true,
      variant: 'outlined',
      typographyVariant: 'subtitle2',
    },
  ]

  const handleClick = (e) => {
    console.log('clicked, validating')
    // after validation, user from local state should be ready to be saved
    dispatch(registerNewUser(user))
  }

  const form = (
    <div>
      {buildFields(headerField)}
      <form className={isDesktop ? classes.rootDesktop : classes.rootMobile}>
        {buildFields(fieldsData)}
        <DatePickers />
        {buildFields(phoneField)}
        <CountryField />
        {buildFields(zipCodeField)}
      </form>
      <TermsAndNews />
      <Button label="REGISTER" handleSubmit={handleClick} />
    </div>
  )

  return form
}
