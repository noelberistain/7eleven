import React from 'react'
import { FormField } from '../formField'

export const buildFields = (data = []) => {
  const listOfFields = data.map((elem) => (
    <FormField
      key={elem.content}
      content={elem.content}
      placeHolder={elem.placeHolder}
      required={elem.required}
      typographyVariant={elem.typographyVariant}
      value={elem.value}
      variant={elem.variant}
      withInputField={elem.withInputField}
    />
  ))
  return listOfFields
}
