// TODO import Logo and Title
import React from 'react'
import { Logo } from '../logo'
import { Title } from '../Title'
import { Grid } from '../common'
import { makeStyles } from '@material-ui/styles'

const useStyles = makeStyles({
  root: {
    margin: 'auto',
  },
})
export const Header = () => {
  const classes = useStyles()

  return (
    <Grid className={classes.root} container justify="center">
      <Logo />
      <Title />
    </Grid>
  )
}
