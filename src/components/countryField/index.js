import React from 'react'
import { Box, Typography } from '../common'
import { CountryCheckBox } from './CountryRadioButton'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  container: {
    margin: '8px 16px',
  },
  textField: {
    width: '90%',
  },
}))

export const CountryField = () => {
  const classes = useStyles()
  return (
    <Box className={classes.container}>
      <Typography content="Country *" variant="subtitle2" />
      <CountryCheckBox />
    </Box>
  )
}
