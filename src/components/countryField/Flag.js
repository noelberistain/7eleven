import React from 'react'
import { CardMedia } from '../common'
import { makeStyles } from '@material-ui/core'
import { useTheme } from '@material-ui/core/styles'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  flagDesktop: {
    width: '3.125rem',
    height: '1.875rem',
  },
  flagMobile: {
    width: '3.125rem',
    height: '1.875rem',
  },
}))

export const Flag = ({ image }) => {
  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  const classes = useStyles()
  return (
    <CardMedia
      className={isDesktop ? classes.flagDesktop : classes.flagMobile}
      image={image}
    />
  )
}
