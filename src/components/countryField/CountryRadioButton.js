import React from 'react'
import { FormControlLabel, Radio, RadioGroup } from '../common'
import { Flag } from './Flag'
import CA from '../../assets/images/ic-canada-desktop@2x.jpg'
import US from '../../assets/images/ic-usa-desktop@2x.jpg'
import { makeStyles } from '@material-ui/core'

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'row',
  },
})

export const CountryCheckBox = () => {
  const [value, setValue] = React.useState('')

  const classes = useStyles()

  const handleChange = (event) => {
    setValue(event.target.value)
  }

  return (
    <RadioGroup
      className={classes.root}
      aria-label="country"
      value={value}
      onChange={handleChange}
    >
      <FormControlLabel
        value="canada"
        control={<Radio />}
        label={<Flag image={CA} />}
      />
      <FormControlLabel
        value="usa"
        control={<Radio />}
        label={<Flag image={US} />}
      />
    </RadioGroup>
  )
}
