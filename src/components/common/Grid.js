import React from 'react'
import { Grid as MUIGrid } from '@material-ui/core'

export const Grid = (props) => <MUIGrid {...props} />
