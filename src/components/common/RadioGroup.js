import React from 'react'
import { RadioGroup as MUIRadioGroup } from '@material-ui/core'

export const RadioGroup = (props) => {
  return <MUIRadioGroup {...props} />
}
