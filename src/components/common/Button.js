import React from 'react'
import { Button as MUIButton, makeStyles } from '@material-ui/core'
import { Box } from './Box'

const useStyles = makeStyles((theme) => ({
  root: { display: 'flex', marginTop: theme.spacing(3) },
  button: { backgroundColor: '#206FCC', margin: 'auto' },
}))

export const Button = (props) => {
  const classes = useStyles()
  const { label, color, variant = 'contained', handleSubmit, ...rest } = props
  return (
    <Box className={classes.root}>
      <MUIButton
        className={classes.button}
        color={color}
        variant={variant}
        onClick={handleSubmit}
        {...rest}
      >
        {label}
      </MUIButton>
    </Box>
  )
}
