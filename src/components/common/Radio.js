import React from 'react'
import { Radio as MUIRadio } from '@material-ui/core'

export const Radio = (props) => {
  return <MUIRadio {...props} />
}
