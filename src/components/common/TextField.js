import React from 'react'
import { TextField as MUITextField } from '@material-ui/core'

export const TextField = (props) => {
  const { handleChange, variant, ...rest } = props
  return <MUITextField {...rest} variant={variant} onChange={handleChange} />
}
