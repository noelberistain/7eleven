import React from 'react'
import { CardMedia as MUICardMedia } from '@material-ui/core'

export const CardMedia = (props) => <MUICardMedia {...props} />
