import React from 'react'
import { Typography as MUITypography } from '@material-ui/core'
import { Box } from './Box'

export const Typography = (props) => {
  const {
    color = 'primary',
    component = 'p',
    content,
    typographyVariant = 'subtitle1',
    handleClick,
    ...rest
  } = props
  return (
    <>
      {content && (
        <MUITypography
          color={color}
          component={component}
          variant={typographyVariant}
          onClick={handleClick}
          {...rest}
        >
          <Box color={color}>{content}</Box>
        </MUITypography>
      )}
    </>
  )
}
