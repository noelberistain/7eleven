import React from 'react'

import { Box as MUIBox } from '@material-ui/core'

export const Box = (props) => <MUIBox {...props} />
