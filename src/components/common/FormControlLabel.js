import React from 'react'
import { FormControlLabel as MUIFormControlLabel } from '@material-ui/core'

export const FormControlLabel = (props) => {
  return <MUIFormControlLabel {...props} />
}
