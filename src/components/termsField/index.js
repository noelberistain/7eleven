import React from 'react'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import FormControl from '@material-ui/core/FormControl'
import FormGroup from '@material-ui/core/FormGroup'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import FormHelperText from '@material-ui/core/FormHelperText'
import Checkbox from '@material-ui/core/Checkbox'
import { Typography } from '../common'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    padding: 0,
  },
  formControlDekstop: {
    margin: '0 auto',
    maxWidth: '32.25rem',
  },
  formControlMobile: {
    margin: '4px auto',
    maxWidth: '16.25rem',
  },
}))

export const TermsAndNews = () => {
  const classes = useStyles()

  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  const [state, setState] = React.useState({
    terms: true,
    news: false,
  })

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked })
  }

  const { terms, news } = state
  const error = terms === false

  return (
    <FormControl
      required
      error={error}
      component="fieldset"
      className={classes.root}
    >
      <FormGroup>
        <FormControlLabel
          className={
            isDesktop ? classes.formControlDekstop : classes.formControlMobile
          }
          control={
            <Checkbox
              checked={terms}
              onChange={handleChange}
              name="terms"
              color={error ? 'primary' : 'secondary'}
            />
          }
          label={
            <Typography
              color={error ? 'secondary' : 'primary.light'}
              content="Yes, I accept the Terms & Conditions and Privacy Policy"
              typographyVariant="subtitle2"
            />
          }
        />
        <FormControlLabel
          className={
            isDesktop ? classes.formControlDekstop : classes.formControlMobile
          }
          control={
            <Checkbox
              checked={news}
              onChange={handleChange}
              name="news"
              color="primary"
            />
          }
          label={
            <Typography
              color="primary.light"
              content="Yes, I'd like to receive news and special offers"
              typographyVariant="subtitle2"
            />
          }
        />
      </FormGroup>
      {error && (
        <FormHelperText>
          - You must accept the terms & conditions and privacy policy
        </FormHelperText>
      )}
    </FormControl>
  )
}
