import React from 'react'
import { Header } from '../header'
import { RegisterForm } from '../registerForm'

export const Register = () => (
  <>
    <Header />
    <RegisterForm />
  </>
)
