import React from 'react'
import { IconButton, makeStyles } from '@material-ui/core/'
import HelpIcon from '@material-ui/icons/Help'
import Tooltip from '@material-ui/core/Tooltip'

const useStyles = makeStyles({
  toolTip: { padding: 0, width: 22, height: 22 },
})

export const Help = () => {
  const classes = useStyles()
  return (
    <Tooltip
      className={classes.toolTip}
      title="Tooltip for the help button"
      placement="top"
    >
      <IconButton color="primary">
        <HelpIcon />
      </IconButton>
    </Tooltip>
  )
}
