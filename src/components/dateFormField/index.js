import React from 'react'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import { Box, TextField } from '../common'
import { Help } from '../helpTooltip'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  containerMobile: {
    margin: theme.spacing(1.875),
    maxWidth: '16.25rem',
    height: '2.5rem',
  },
  textFieldMobile: {
    width: '90%',
  },
  containerDesktop: {
    margin: '8px 16px',
    maxWidth: '32.25rem',
    height: '2.5rem',
  },
  textFieldDesktop: {
    width: '95%',
  },
}))

export const DatePickers = () => {
  const classes = useStyles()

  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))

  return (
    <Box
      className={isDesktop ? classes.containerDesktop : classes.containerMobile}
    >
      <TextField
        className={
          isDesktop ? classes.textFieldDesktop : classes.textFieldMobile
        }
        id="date"
        label="Birthdate *"
        type="date"
        defaultValue="2017-05-24"
        InputLabelProps={{
          shrink: true,
        }}
      />
      <Help />
    </Box>
  )
}
