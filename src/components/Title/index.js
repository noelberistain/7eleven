import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Typography from '@material-ui/core/Typography'
import { makeStyles, useTheme } from '@material-ui/core/styles'
import { useMediaQuery } from '@material-ui/core'

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
    width: '100%',
  },
  toolbarMobile: {
    background: 'linear-gradient(90deg, #48ADB6 20%, #9182C6 80%)',
    color: '#FFFFFF',
    height: 103,
    margin: 'auto',
    maxWidth: 320,
  },
  toolbarDesktop: {
    background: 'linear-gradient(90deg, #48ADB6 20%, #9182C6 80%)',
    color: '#FFFFFF',
    height: 179,
    margin: 'auto',
    maxWidth: 1203,
  },
  title: {
    fontWeight: 'bold',
    margin: 'auto',
    opacity: '100%',
  },
}))

export const Title = () => {
  const classes = useStyles()
  const theme = useTheme()
  const isDesktop = useMediaQuery(theme.breakpoints.up('desktop'))
  const Title = (
    <AppBar
      className={isDesktop ? classes.toolbarDesktop : classes.toolbarMobile}
      position="static"
    >
      <Typography className={classes.title} variant="h2">
        REGISTER
      </Typography>
    </AppBar>
  )

  return <div className={classes.root}>{Title}</div>
}
