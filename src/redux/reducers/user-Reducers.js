import { LOGIN_USER, LOGOUT_USER } from '../constants'

const user = {}

export const userReducer = (state = user, action) => {
  switch (action.type) {
    case LOGIN_USER:
      //action.payload should be an object/data for new user
      return { ...state, ...action.payload }
    default:
      return state
  }
}
