import { combineReducers } from 'redux'
import { usersReducer } from './usersReducer'
import { userReducer } from './user-Reducers'

export default combineReducers({ users: usersReducer, user: userReducer })
