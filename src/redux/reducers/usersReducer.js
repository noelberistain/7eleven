import { REGISTER_NEW_USER } from '../constants'

const initialState = []

export const usersReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER_NEW_USER:
      //action.payload should be an object/data for new user
      return [...state, action.payload]
    default:
      return state
  }
}
