import { createStore, compose } from 'redux'
import rootReducer from '../reducers/rootReducer'

export const configureStore = () => {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
  return createStore(rootReducer, composeEnhancers())
}
