import { REGISTER_NEW_USER } from '../constants'

export const registerNewUser = (payload) => ({
  type: REGISTER_NEW_USER,
  payload,
})

export { registerNewUser }
