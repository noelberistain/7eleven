import { LOGIN_USER } from '../constants'

export const loginUser = (payload) => ({
  type: LOGIN_USER,
  payload,
})

export { loginUser }
